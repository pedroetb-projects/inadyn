# inadyn

In-a-Dyn is a dynamic DNS client with multiple SSL/TLS library support

## Configuration

You must provide a configuration file at `./deploy/inadyn.conf`. Following example shows how to define check ip period in seconds, two different accounts to update DNS registers on OVH and another one on Dynu.

```text
period = 60

provider default@ovh.com:1 {
    username = user
    password = changeme
    hostname = { subdomain.change.me, app-example.com }
    checkip-server = api.ipify.org
}

provider default@ovh.com:2 {
    username = anotheruser
    password = changemetoo
    hostname = another-app.com
    checkip-server = api.ipify.org
}

provider default@dynu.com {
    username = user
    password = changeme
    hostname = subdomain.dynu.net
    checkip-server = api.ipify.org
}
```

When using GitLab CI file variable to define it, set `INADYN_CONF` for each environment using configuration content as value. Then, CI jobs for deployment will pick the file for current environment.
